<?php

namespace Training\Gain\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class EchoCommand extends \Symfony\Component\Console\Command\Command
{
    protected function configure()
    {
        $this->setName('training:gain:echo');
        $this->addOption('text', null, InputOption::VALUE_OPTIONAL, 'Text which will be echoed');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = $input->getOption('text');
        $output->writeln($message);
        return \Magento\Framework\Console\Cli::RETURN_SUCCESS;
    }
}